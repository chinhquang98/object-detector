# Phone models Detector using CreateML

In this tutorial, we will use Create Ml to detect phone models (like iPhone, iPad, Samsung,..)

## Getting Started

CreateML is a powerful, friendly UI tool for training a machine learning model such as image classifier or object detector. 

### Prerequisites

You need to install MacOS 10.15.0 or later because Create ML in lower version of MacOS just supporting playGround and Image Classifier.

Open Spotlight search, type Create ML, you will see Create ML is already installed in you operation system.


### Prepare image dataset for training


